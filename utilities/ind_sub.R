ind2sub = function(indStack, dimOfRow){
  c = ceiling( indStack / dimOfRow );
  r = ( (indStack - 1) %% dimOfRow ) + 1
  return(cbind(r, c))
}

sub2ind = function(r, c, dimOfRow){
  ind = (c - 1) * dimOfRow + r;
  return(ind)
}