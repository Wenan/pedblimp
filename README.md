# README #

PedBLIMP: a tool for genotype imputation of individuals with pedigree information.

### What is this repository for? ###

* This tool can use both the relatedness information between family members and correlations among genotypes. Family members can have completely missing genotypes or very different density of genotype markers. It is implemented in R.
* 0.2.0

### How do I get set up? ###

* Installation: Simply download it and unzip it.
* Dependencies: R packages: Matrix, optparse, kinship2. To estimate the locus specific relatedness, it depends on *gl_auto* in the MORGAN package (http://www.stat.washington.edu/thompson/Genepi/MORGAN/Morgan.shtml). See more in the manual about generating precomputed panel data and input file formats

### How to run tests ###
* In Linux, type

```
#!bash

bash ./testPedBLIMP.sh
```

* For Windows, change testPedBLIMP.sh to testPedBLIMP.bat and run it in a command line window

### Who do I talk to? ###

* Please feel free to contact me: Chen.Wenan@mayo.edu. Any problems, suggestions, comments, jokes, ... are welcome!