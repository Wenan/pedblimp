# estimate locus specific relatedness, make sure the gl_auto from MORGAN is installed 
# and change the path "../../tools/Morgan/MORGAN_V311_Release/Autozyg/gl_auto" to where gl_auto is installed

Rscript estimateIBDFromInferredIV.R ./examples/panel.legend ./examples/panel.map 5 ./examples/data_0.1cM.dosage ./examples/ped ../../tools/Morgan/MORGAN_V311_Release/Autozyg/gl_auto ./examples/IVInfer estimated 6000 30 100 

# use the estimated locus specific relatedness
Rscript pedBLIMP.R -H ./examples/CEU.hap -L ./examples/panel.legend -M ./examples/panel.map -N 11418 -d ./examples/precompute_cov_mean -i ./examples/data_0.1cM.dosage -t "IVInferred_blockwise" -k ./examples/IVInfer/ibdSNP.estimated.RData -o ./examples/data_0.1cM.dosage.estimated.imputed -s 20000000 -e 25000000
