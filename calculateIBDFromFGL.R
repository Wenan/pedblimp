# calculateIBDFromFGL.R

# description
# This function calculate the IBD matrix based on the FGL data for each SNP region

# input
# FGLListMa: the maternal FGL list for all individuals
# FGLListPa: the paternal FGL List for all individuals

# output
# ibd: it has two elements. The first one an 3*nIndiv*nIndiv array which stores the 
# IBD matrix for SNPs befor each crossover. The second element shows the positions 
# where crossover happens thus IBD matrix changes.

# auxiliary function to summarize the crossover positions and gene labels of an individual
getCrossoverAndGeneLabels = function(index, FGLMa, FGLPa){
  count = FGLMa[2] + FGLPa[2];
  if(count == 0){
    return(NULL);
  }
  if( FGLMa[2] == 0 ){ # FGLPa[2]>0
    changeItems = matrix(NA, FGLPa[2], 4);
    for ( i in 1:FGLPa[2]){
      changeItems[i, ] = c(FGLPa[2+i*2-1], index, FGLMa[1], FGLPa[2 + i*2]);
    }
  }
  if( FGLPa[2] == 0 ){ # FGLMa[2]>0
    changeItems = matrix(NA, FGLMa[2], 4);
    for ( i in 1:FGLMa[2]){
      changeItems[i, ] = c(FGLMa[2+i*2-1], index, FGLMa[2 + i*2], FGLPa[1]);
    }
  }
  if( FGLMa[2] > 0 && FGLPa[2] > 0 ){
    # modify the data to make it consistent with the following elements: even elements show the gene labels
    FGLMa[2] = FGLMa[1]; FGLPa[2] = FGLPa[1]; 
    curPosMa = 3; curPosPa = 3;
    i = 1; endMa = 0; endPa = 0; changeItems = matrix(NA, count, 4);
    
    while( endMa == 0 || endPa == 0){
      if( FGLMa[curPosMa] == FGLPa[curPosPa ] ){
        changeItems[i, ] = c(FGLMa[curPosMa], index, FGLMa[curPosMa+1], FGLPa[curPosPa+1]);
        if( curPosMa == length(FGLMa)-1 ){
          endMa = 1;
        } else {
          curPosMa = curPosMa + 2;
        }
        if( curPosPa == length(FGLPa)-1 ){
          endPa = 1;
        } else {
          curPosPa = curPosPa + 2;
        }        
        
      } else if( (endMa == 0 && FGLMa[curPosMa] < FGLPa[curPosPa ]) || endPa == 1 ){
        if(endPa == 1){
          changeItems[i, ] = c( FGLMa[curPosMa], index, FGLMa[ curPosMa + 1 ], FGLPa[ curPosPa + 1]); # use the curPosPa + 1
        } else {
          changeItems[i, ] = c( FGLMa[curPosMa], index, FGLMa[ curPosMa + 1 ], FGLPa[ curPosPa - 1]);
        }
          
        if( curPosMa == length(FGLMa)-1 ){
          endMa = 1;
        } else {
          curPosMa = curPosMa + 2;
        }
      } else if( (endPa == 0 && FGLMa[curPosMa] > FGLPa[curPosPa ]) || endMa == 1 ){
        if( endMa == 1){
          changeItems[i, ] = c( FGLPa[curPosPa ], index, FGLMa[ curPosMa + 1 ], FGLPa[ curPosPa + 1]); # use the curPosMa + 1
        } else {
          changeItems[i, ] = c( FGLPa[curPosPa ], index, FGLMa[ curPosMa - 1 ], FGLPa[ curPosPa + 1]);
        }
        
        if( curPosPa == length(FGLPa)-1 ){
          endPa = 1;
        } else {
          curPosPa = curPosPa + 2;
        }        
      }
      
      i = i + 1;   
      
    }
    
    changeItems = changeItems[1:(i -1), , drop = F];
  }
  
  return(changeItems)
}

# This function calculate the ibd from two 2*1 vectors storing the 2 gene copies
# from maternal and paternal parents
# To speedup the process, the return value is 4*tureIBD as an integer
ibdFromGeneLabels = function(curLabels1, curLabels2){
  ibd = integer(1); 
  if(curLabels1[1] == curLabels2[1]){
    ibd = ibd + 1;
  } 
  if(curLabels1[1] == curLabels2[2]){
    ibd = ibd + 1;
  }
  if(curLabels1[2] == curLabels2[1]){
    ibd = ibd + 1;
  }  
  if(curLabels1[2] == curLabels2[2]){
    ibd = ibd + 1;
  }  
  return(ibd);
}


calculateIBDFromFGL = function(FGLListMa, FGLListPa){
  nIndiv = length(FGLListMa);
  curLabels = matrix(NA, nIndiv, 2)
  
  # estimate the size of changeArray
  ncross = 0;
  for (j in 1:nIndiv){
    ncross = ncross + FGLListMa[[j]][2] + FGLListPa[[j]][2];
  }
  
  if(ncross != 0){
    # generate the matrix which stores the crossover points
    changeArray = matrix(NA, ncross, 4);
  }
  
  count = 1;
  for (j in 1:nIndiv){
#     print(j)
    curLabels[j, ] = c(FGLListMa[[j]][1], FGLListPa[[j]][1]);
    change_j = getCrossoverAndGeneLabels(j, FGLListMa[[j]], FGLListPa[[j]]);
    
    # <debug>
#     print(FGLListMa[[j]])
#     print(FGLListPa[[j]])
#     print(change_j)
#     print(count)
    # </debug>
    
    if(!is.null(change_j)){
      changeArray[count:(dim(change_j)[1]+count-1), ] = change_j;
      count = count + dim(change_j)[1];
    }
    
  }
  
  if(ncross != 0 ){
    changeArray = changeArray[1:(count-1), , drop=F];
    
    # sort changeArray
    permIndex = order(changeArray[, 1]);
    changeArray = changeArray[permIndex, , drop=F];
    
    # merge identical positions
    crossoverPositions = unique(changeArray[, 1]);
    
    changeList = list(); # list version of changeArray
    changeList[[length(crossoverPositions)]] = NULL; # extend the length to the number of crossovers
    p1 = 1; p2 = 0; p3 = 1;
    while (p1 <= dim(changeArray)[1]){
      p2 = p1;
      # search forward identical crossover positions
      while( changeArray[p2, 1] == changeArray[p1, 1] ){
        # add the items into the list
        if(is.null(changeList[p3][[1]])){
          changeList[[p3]] = changeArray[p2, , drop=F];
        } else {
          changeList[[p3]] = rbind(changeList[[p3]], changeArray[p2, ]);
        }
        
        p2 = p2 + 1;
        
        if(p2 > dim(changeArray)[1]){
          break;
        } 
        
      }
      
      p3 = p3 + 1;
      p1 = p2 ;
    }
    
  }
  
  # calculate the IBD matrix based on the gene labels on the first SNP, i.e., curLabels
  
  ibdM1 = matrix(NA, nIndiv, nIndiv); # only fill in the upper triangle
  storage.mode(ibdM1) = "integer"; # It stores 4 * tureIBD values as integers
  #diag(ibdM1) = 2;  # this only works for out-bred pedigrees
  for ( i in 1: ( nIndiv ) ) {
    for ( j in ( i : nIndiv ) ){    # include inbreeding too, so starts with (i,i)
      ibdM1[i, j] = ibdFromGeneLabels(curLabels[i, ], curLabels[j, ]);
      
      # <debug>
      #print(c(i, j, ibdM1[i, j]))
      # </debug>
    }
  }
  
  ibd = list();
  
  if( ncross == 0 ){
    ibd$array = array(NA, dim = c(nIndiv, nIndiv, 1) );
    storage.mode(ibd$array) = "integer";
    ibd$array[ , , 1] = ibdM1;
    ibd$crossovers = c(1);
    return(ibd);
  }
  
  # The first position is all always 1
  crossoverPositions = c(1, crossoverPositions);
  ibd$crossovers = crossoverPositions;
  
  # go through all crossover positions to update the ibd matrix  
  ibd$array = array(NA, dim = c(nIndiv, nIndiv, length(ibd$crossovers)));
  storage.mode(ibd$array) = "integer";
  ibd$array[ , , 1] = ibdM1;
  
  for (i in 2:length(crossoverPositions)){
    
    # <debug>
#     if( i == 81 ){
#       print(i)
#     }
    # </debug>
    
    change_i = changeList[[i-1]];
    indivToUpdate = change_i[, 2]; # individual indices
    # update curLabels
    curLabels[indivToUpdate, ] = change_i[, c(3,4)]; # gene labels
    # update IBD matrix
    for (j in 1:length(indivToUpdate)){
      ind_j = indivToUpdate[j];
      
      for ( k in 1: ( nIndiv ) ){
        if( k >= ind_j ){ # update row           # the euqal sign includes inbreeding cases
          ibdM1[ind_j, k] = ibdFromGeneLabels(curLabels[k, ], curLabels[ind_j, ]);
        }
        if( k < ind_j ){ # update column
          ibdM1[k, ind_j] = ibdFromGeneLabels(curLabels[k, ], curLabels[ind_j, ]);
        }
      }      
    }
    # store the updated IBD matrix
    ibd$array[ , , i] = ibdM1;
    
  }
  
  return(ibd)

} 
