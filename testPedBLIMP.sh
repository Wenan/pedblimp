# precompute the covariance and mean from panel data
Rscript pedBLIMP.R -H ./examples/CEU.hap -L ./examples/panel.legend -M ./examples/panel.map -N 11418 --precompute_cov_mean -d ./examples/precompute_cov_mean --chunk_snps=300 --buffer_snps=300

# use the expected relatedness
Rscript pedBLIMP.R -H ./examples/CEU.hap -L ./examples/panel.legend -M ./examples/panel.map -N 11418 -d ./examples/precompute_cov_mean -i ./examples/data_0.1cM.dosage -t "expected" -k ./examples/ped -o ./examples/data_0.1cM.dosage.expected.imputed -s 20000000 -e 25000000

# use the true relatedness
Rscript pedBLIMP.R -H ./examples/CEU.hap -L ./examples/panel.legend -M ./examples/panel.map -N 11418 -d ./examples/precompute_cov_mean -i ./examples/data_0.1cM.dosage -t "truth" -k ./examples/2xkinship.truth -o ./examples/data_0.1cM.dosage.truth.imputed -s 20000000 -e 25000000
